var FizzBuzz =  function FizzBuzz(){
    FizzBuzz.prototype.run= function input(input){

        function isMultipleOf3(input){
            if(input % 3 === 0)
                return true;
            return false;
        }
        function isMultipleOf5(input){
            if(input % 5 === 0)
                return true;
            return false;
        }

        if(isMultipleOf3(input) && isMultipleOf5(input)){
            return "FizzBuzz";
        }
        if(isMultipleOf3(input)){
            return "Fizz";
        }
        if(isMultipleOf5(input)){
            return "Buzz";
        }
        return input;
    }
}
module.exports=FizzBuzz;