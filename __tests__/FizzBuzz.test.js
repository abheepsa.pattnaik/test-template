var FizzBuzz = require("../src/FizzBuzz.js");

describe('FizzBuzz', function () {
    describe('run', function () {
        it('should be the number itself  for non multiple numbers of 3 and 5', function () {
            var fizzBuzz= new FizzBuzz();
            var result = fizzBuzz.run(1);
            expect (result).toBe(1);
        });

        it('should Fizz  for 3 ', function () {
            var fizzBuzz= new FizzBuzz();
            var result = fizzBuzz.run(3);
            expect (result).toBe("Fizz");
        });
        it('should Fizz  for multiple of 3 ', function () {
            var fizzBuzz= new FizzBuzz();
            var result = fizzBuzz.run(12);
            expect (result).toBe("Fizz");
        });
        it('should Fizz  for 5 ', function () {
            var fizzBuzz= new FizzBuzz();
            var result = fizzBuzz.run(5);
            expect (result).toBe("Buzz");
        });
        it('should Buzz  for multiple of 5 ', function () {
            var fizzBuzz= new FizzBuzz();
            var result = fizzBuzz.run(25);
            expect (result).toBe("Buzz");
        });
        it('should be FizzBuzz for multiple of 3 and 5 ', function () {
            var fizzBuzz= new FizzBuzz();
            var result = fizzBuzz.run(30);
            expect (result).toBe("FizzBuzz");
        });

    });

});